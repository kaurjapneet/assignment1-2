import static org.junit.Assert.*;

import org.junit.Test;

public class BankTest {

		@Test
		public void testCreateANewBank() {
			//B1: When a new Bank is created, it has 0 customers.
			//1. Create a new bank account
			Bank bank = new Bank();
	        //2. Get number of customers
	        int NumberOfCustomers = bank.getNumberOfCustomers();
	        //3.Assert
	        assertEquals(0,NumberOfCustomers);
		    
	        
	}

}
