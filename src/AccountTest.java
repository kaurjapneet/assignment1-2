import static org.junit.Assert.*;

import org.junit.Test;

public class AccountTest {

		@Test
		public void testCreateNewAccount() {
			//A1: All new accounts are initialized with 0 dollars in the account.

			//1. Create a new bank account
			Account account = new Account(100);
			
			//2. Check the amount of money in the account
			int actualBalance=account.balance();
			
			//3. Expected result=money=0
			//4. Compare actual to expected result
			assertEquals(0,actualBalance);
			//fail("Not yet implemented");
		
		}
			@Test
		     public void testMakeADeposit(){
				 //A2: You can withdraw or deposit money from an account

		    	 //1. Make an account
		    	 Account account = new Account(500);
		    	 int initialBal = account.balance();
		    	 //2. Deposit some money from an account
		    	 account.deposit(100);
		    	 //3. Check balance
		    	 int finalbalance = account.balance();
		    	//4. Assert
		    	assertEquals(initialBal + 100,finalbalance);
		    	 
		}
			@Test
		     public void testMakeAWithdrawl(){
				//A3: Withdrawals can only occur if the account has sufficient funds to cover the withdrawal.  The account�s balance is updated immediately after a withdrawal.
		    	//1. Make an account
		    	 Account account = new Account(300);
		    	//2. Withdraw some money from an account
		    	 account.withdraw(100);
		    	 //3. Check balance
		    	 int finalbalance = account.balance();
		    	//4. Assert
		    	assertEquals(200,finalbalance);
		    	
	     }
			
     }		

	


